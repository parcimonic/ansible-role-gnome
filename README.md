# Gnome install with Ansible

This is my very opinionated Gnome install role. Even though there are mentions
of `ansible-galaxy` (the tool), I don't have any plans to publish it to Ansible
Galaxy (the repository).

## How to use

You must set the `gnome_user` variable to your username in order to apply Gnome
settings to the correct user.

## How to test

You'll need [Molecule](https://molecule.readthedocs.io/en/latest/installation.html),
the `podman` plugin for Molecule and `ansible-lint`.

Run `molecule test` in the root path of this repository.

Starting from version 1.0.0, the default Molecule test OS is Arch Linux. To run
tests on Ubuntu, set the env var `ROLE_TEST_OS` to `ubuntu`.

## Should I use it?

This is not meant to be a flexible/customizable role, so if you're looking
for that, check out the collections/roles in [Ansible Galaxy](https://galaxy.ansible.com/search?deprecated=false&keywords=gnome&order_by=-relevance&page=1).

This role sets Caps Lock as another Ctrl key, so be mindful of this when making
your decision.
